#!/bin/bash

#source: https://www.reddit.com/r/Fedora/comments/gbhdda/configure_trackball_mouse_without_xorg/
gsettings set org.gnome.desktop.peripherals.trackball middle-click-emulation true
gsettings set org.gnome.desktop.peripherals.trackball scroll-wheel-emulation-button 8

