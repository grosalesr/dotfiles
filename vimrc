" Basic Setup
set nocompatible        " don't bother to work as vi

"Show file fullpath
set statusline+=%F

" Enable syntax and plugins
syntax enable
filetype plugin on

" Reload file if it was externally modified
set autoread
au CursorHold * checktime

" automatically change directory to the current file location
set autochdir

" Unicode
" https://vim.fandom.com/wiki/Working_with_Unicode
set encoding=utf-8      " change output encoding shown in the terminal


" Visual options

" Use true or 256 colors terminal emulators
" https://gist.github.com/XVilka/8346728
if &term =~? 'rxvt-unicode'
	set t_Co=256
else
	if exists('+termguicolors')
  		let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  		let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  		set termguicolors
	endif
endif

" Colorscheme
colo base16-gruvbox-dark-medium

" Line numbers
set number              " display line number
set relativenumber      " enable relative number

set ruler               " show the cursor position(line,column number)
set cursorline          " horizontal highlight/underline (depending on colorscheme) current line

set showmatch           " highlight matching [{()}]
set lazyredraw          " redraw only when we need to
set laststatus=2        " always display the status line, even if only one window is displayed


" Indentation
" https://vim.fandom.com/wiki/Indenting_source_code
set smarttab            " affects how tab are interpreted depending where the cursor is
set tabstop=4           " number of visual spaces per tab
set expandtab           " turns tabs into spaces
set shiftwidth=4        " amount of spaces for nested code
set softtabstop=4       " number of spaces in tab when editing
filetype indent on      " load filetype-specific indent files
set autoindent


" Autocomplete & Finding Files
" - :b substring jumps between open buffers
" - :find someKeyword, hit tab to navigate thru matches
"   -- use * to make it fuzzy
set path+=**            " Search down into subfolders(rescursive search)

" Enables autocomplete menu
" - ctrl+n, next result
" - ctrl+p, previous result
" - ctrl+x+f, for filenames
set wildmenu            " command-line completion operates in enhanced mode
set wildmode=list:longest,full  " Turns on tab complete, think of autocomplete


" Code folding
" https://vim.fandom.com/wiki/Folding
set foldenable          " enable folding
set foldmethod=indent   " folding method
set foldopen-=block     " do not open a fold if it has an empty line, for { and } motions
set foldcolumn=0        " indicate folds and their nesting level
set foldnestmax=10      " 10 nested fold max
set foldlevelstart=5    " all folds are closed by default
nnoremap <space> za     " space open/closes fold


" Search
set hlsearch        " highlight searches
set incsearch       " do incremental searching
set ignorecase      " ignore case when searching


" Utilities

" Automatically deletes all trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Remove all lines that are empty
map -  :g/^$/d

" Create tags for file(ctags package must be installed)
" - ctrl+] to jump to tag under the cursor
" - ctrl+t to jump back to the tag stack
" - g+ctrl+] for global definitions
command! MakeTags !ctags -R .

" Automatic bracket closing
" https://stackoverflow.com/questions/21316727/automatic-closing-brackets-for-vim
    ino " ""<left>
    ino ' ''<left>
    ino ( ()<left>
    ino [ []<left>
    ino { {}<left>
    ino {<CR> {<CR>}<ESC>O
    ino {;<CR> {<CR>};<ESC>O


" Preferences for various file formats
autocmd FileType yaml setlocal ai ts=2 sts=2 sw=2 et


" Plugins
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

"Make sure you use single quotes

" vim-surround
Plug 'https://github.com/tpope/vim-surround'

" tagbar
Plug 'https://github.com/majutsushi/tagbar'

" NERDTree
" t: open the selected file in a new tab and change the active buffer
" T: open the selected file in a new tab
" s: open the selected file in a vertical split window
" i: open the selected file in a horizontal split window
" I: toggle hidden files
" R: refresh the tree, useful if files change outside of vim
" m: show menu
" ?: show help
Plug 'https://github.com/scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" Conquer of Completion (Intellisense Engine)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Initialize the plugin system
call plug#end()

" Plugins Configuration

" tagbar
nmap <F8> :TagbarToggle<CR>

" NERDTree
map <F2> :NERDTreeToggle<CR>
