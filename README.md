## Table of Contents
- [Abstract](#abstract)
- [Sway](#sway)
- [Alacritty](#alacritty)
    - [Installation](#installation)
    - [Configuration](#configuration)
- [Themes](#themes)
    - [Base16](#base16)

## Abstract

dot files and theming, place the folders in the following location as appropiate:

* **$HOME**
* **$HOME/.config**

## [Sway](https://github.com/swaywm/sway)

* [mako](https://github.com/emersion/mako): Notifications Daemon
* [Kanshi](https://github.com/emersion/kanshi): Output profiles on hotplug

## [Alacritty](https://github.com/alacritty/alacritty)

* fonts
    * list installed fonts: `fc-list | grep -i [font]`

## Themes

* [Base16](https://github.com/chriskempson/base16)
    * [vim](https://github.com/chriskempson/base16-vim)
    * [mako](https://github.com/Eluminae/base16-mako)
* [Dracula](https://draculatheme.com/)
* [Monokai](https://www.monokai.nl/)
* [xresources](https://github.com/binaryplease/base16-xresources)
    * **urxvt**: After changes are made, execute `xrdb -merge ~/.Xresources` in order to load the new configuration.
