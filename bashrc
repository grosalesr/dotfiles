# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

# git prompt
. /usr/share/git-core/contrib/completion/git-prompt.sh

# neovim as default editor
export EDITOR=nvim

export PROMPT_DIRTRIM=2 # shorten paths displayed in prompt
export PROMPT_DIRECTORY='\w$(declare -F __git_ps1 &>/dev/null && __git_ps1 " (%s)")'

# KWin scripting
#export QT_LOGGING_RULES='js=true; kwin_scripting=true'

# aliases
alias remote-app="waypipe -c lz4=9 ssh -X"

alias ssh="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
alias scp="scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
alias diff="diff --color --suppress-common-lines --ignore-blank-lines --suppress-blank-empty -y"

alias tailscale-up='sudo tailscale up'
alias tailscale-down='sudo tailscale down'

alias virsh-local="virsh -c qemu:///system"
alias virsh-skynet='virsh -c qemu+ssh://sysadmin@skynet/system'

alias backup_files='$HOME/Documents/projects/go/warden/bin/warden backup -f $HOME/Documents/backups/backup_definitions.yml'

# Set up fzf key bindings and fuzzy completion
eval "$(fzf --bash)"
