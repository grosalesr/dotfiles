--  https://github.com/ThePrimeagen/harpoon/tree/harpoon2
-- https://www.reddit.com/r/neovim/comments/18as0nm/harpoon2_branch_lazy_vim_setup/

return {
	"ThePrimeagen/harpoon",

	branch = "harpoon2",
	dependencies = { "nvim-lua/plenary.nvim" },

	config = function()
		require("harpoon"):setup()
	end,

	keys = {
		{"<leader>a", function() require("harpoon"):list():add() end, desc = "harpoon file", },
		{"<C-e>", function() local harpoon = require("harpoon")  harpoon.ui:toggle_quick_menu(harpoon:list()) end, desc = "harpoon quick menu", },
		{"<C-h>", function() require("harpoon"):list():select(1) end, desc = "harpoon to file 1", },
		{"<C-t>", function() require("harpoon"):list():select(2) end, desc = "harpoon to file 2", },
		{"<C-n>", function() require("harpoon"):list():select(3) end, desc = "harpoon to file 3", },
		{"<C-s>", function() require("harpoon"):list():select(4) end, desc = "harpoon to file 4", },

		-- Toggle previous & next buffers stored within Harpoon list
		{"<C-S-g", function() harpoon:list():prev() end},
		{"<C-S-c>", function() harpoon:list():next() end},
	},
}