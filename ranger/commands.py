import os
from ranger.api.commands import Command


class editNewWindow(Command):
    def execute(self):
        # Gets a file passed/selected
        if self.arg(1):
            filename = self.rest(1)
        else:
            filename = self.fm.thisfile.path

        if not os.path.exists(filename):
            self.fm.notify("File does not exists", bad=True)
            return

        cmd = self.getCMD(filename)
        self.fm.run(cmd)

    def tab(self, tabnum):
        return self._tab_directory_content()

    def getCMD(self, filename):
        """
        Based on the DE's terminal emulator & EDITOR (env variable) returns the
        command to open the given file on a new terminal window & EDITOR
        """
        cmdParts = []
        send2BG = "&"
        editor = os.getenv('EDITOR')

        DE = os.getenv('DESKTOP_SESSION')
        if DE == "xfce":
            execFlag = "-x"
            term = "xfce4-terminal"
            cmdParts.append(term)
            cmdParts.append(execFlag)
        else:
            execFlag = "-e"
            term = "alacritty"
            cmdParts.append(term)
            cmdParts.append(execFlag)


        cmd = "{} {} {} {} {}".format(cmdParts[0], cmdParts[1], editor, filename, send2BG)
        return cmd
